<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Pemakaian extends Model
{
    protected $guarded = [];

    public function detail(){
    	return $this->hasMany(DetailPemakaian::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function delete(){
    	$this->detail()->delete();
    	parent::delete();
    }

    public function kode()
    {
        $query = DB::table('pemakaians')->select(DB::raw('RIGHT(pemakaians.nomor,4) as kode', FALSE))
                                         ->orderBy('nomor','DESC')
                                         ->limit(1)
                                         ->get();
        $count = count($query);
        if($count <> 0)
        {
            // $data = $query->toArray();
            $kode = intval($query[0]->kode) + 1;
        }
        else
        {
            $kode = 1;
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "MR-".$kodemax;
        return $kodejadi;
    }
}
