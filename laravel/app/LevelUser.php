<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelUser extends Model
{
    protected $guarded = [];


    public function user(){
        return $this->hasMany('App\LevelUser','level_user_id');
    }
}
