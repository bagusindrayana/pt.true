<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemakaian;
use App\DetailPemakaian;
use App\Gudang;
use App\User;
use App\Unit;
class PemakaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'pemakaian';
        $title = 'Pemakaian';
     
        $pemakaians = Pemakaian::all();

        return view('pemakaian.index',compact('pemakaians','page','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'pemakaian';
        $title = 'Pemakaian';
        $user = User::all();
        $barangs = Gudang::all();
        $units = Unit::all();
        return view('pemakaian.create',compact('page','title','user','barangs','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $pemakaian = new Pemakaian;
        $kode = $pemakaian->kode();
        $g = Gudang::where('kode_barang',$request->kode_barang)->first();
        if($request->jumlah <= $g->jumlah){
            $id = Pemakaian::create([
                'tanggal'=>$request->tanggal,
                'nomor'=>$kode,
                'user_id'=>$request->user_id,
              
            ])->id;
            //$jumlah = $request->qty * $request->harga_satuan;

            
            Gudang::where('kode_barang',$request->kode_barang)->update(['jumlah'=>$g->jumlah-$request->jumlah]);
            DetailPemakaian::create([
                'pemakaian_id'=>$id,
                'uraian'=>$request->uraian,
                'jumlah'=>$request->jumlah,
                'kode_barang'=>$request->kode_barang,
                'kode_unit'=>$request->kode_unit,
                'harga'=>$g->harga
            ]);
            return redirect('pemakaian/'.$id.'/edit');
        }
        else {

            return back();
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'pemakaian';
        $title = 'Pemakaian';
        $user = User::all();
        $barangs = Gudang::all();
        $units = Unit::all();
        $data = Pemakaian::findOrFail($id);
        return view('pemakaian.view',compact('page','title','user','barangs','units','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'pemakaian';
        $title = 'Pemakaian';
        $user = User::all();
        $barangs = Gudang::all();
        $units = Unit::all();
        $data = Pemakaian::findOrFail($id);
        return view('pemakaian.edit',compact('page','title','user','barangs','units','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->tombol == 'ubah') {
            Pemakaian::where('id',$id)->update([
                    'tanggal'=>$request->tanggal,
                    'nomor'=>$request->nomor,
                    
                    'diminta_id'=>$request->diminta_id,
                   
            ]);
        }
        else if($request->tombol == 'tambah_detail'){
            //$jumlah = $request->qty * $request->harga_satuan;
            $g = Gudang::where('kode_barang',$request->kode_barang)->first();
            Gudang::where('kode_barang',$request->kode_barang)->update(['jumlah'=>$g->jumlah-$request->jumlah]);
            $cek = DetailPemakaian::where(['kode_barang'=>$request->kode_barang,'pemakaian_id'=>$id]);
            if($cek->count() > 0){
                $jumlah = $cek->first()->jumlah;
                $cek->update([
                    
                    'uraian'=>$request->uraian,
                    'jumlah'=>$request->jumlah+$jumlah,
                  
                    
                ]);
            }
            else {
                DetailPemakaian::create([
                    'pemakaian_id'=>$id,
                    'uraian'=>$request->uraian,
                    'jumlah'=>$request->jumlah,
                    'kode_barang'=>$request->kode_barang,
                    'kode_unit'=>$request->kode_unit,
                    'harga'=>$g->harga
                ]);
            }
                
        }

        return redirect('pemakaian/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pemakaian::findOrFail($id)->delete();
        return back();
    }
}
