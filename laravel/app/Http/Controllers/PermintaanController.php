<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permintaan;
use App\DetailPermintaan;
use App\User;
use App\Unit;
use App\Barang;
class PermintaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $page = 'permintaan';
        $title = 'Permintaan';
        $permintaans = Permintaan::all();
        return view('permintaan.index',compact('page','title','permintaans'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'permintaan';
        $title = 'Permintaan';
        $user = User::all();
        $units = Unit::all();
        $barangs = Barang::all();
        return view('permintaan.create',compact('page','title','user','units','barangs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permintaan = new Permintaan;
        $kode = $permintaan->kode();
        $nomor = $kode;

        $id = Permintaan::create([
            'tanggal'=>$request->tanggal,
            'nomor'=>$kode,
            'diminta_id'=>$request->diminta_id,
          
        ])->id;
        //$jumlah = $request->qty * $request->harga_satuan;
        DetailPermintaan::create([
            'permintaan_id'=>$id,
            'uraian'=>$request->uraian,
            'jumlah'=>$request->jumlah,
            'kode_barang'=>$request->kode_barang,
            'kode_unit'=>$request->kode_unit,
        ]);
        return redirect('permintaan/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'permintaan';
        $title = 'Permintaan';
        $user = User::all();
        $units = Unit::all();
        $barangs = Barang::all();
        $data = Permintaan::findOrFail($id);

        return view('permintaan.view',compact('page','title','user','units','barangs','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'permintaan';
        $title = 'Permintaan';
        $user = User::all();
        $units = Unit::all();
        $barangs = Barang::all();
        $data = Permintaan::findOrFail($id);

        return view('permintaan.edit',compact('page','title','user','units','barangs','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->tombol == 'ubah') {
            Permintaan::where('id',$id)->update([
                    'tanggal'=>$request->tanggal,
                    'nomor'=>$request->nomor,
                    'diminta_id'=>$request->diminta_id,
                   
            ]);
        }
        else if($request->tombol == 'tambah_detail'){
            //$jumlah = $request->qty * $request->harga_satuan;
            
            DetailPermintaan::create([
                'permintaan_id'=>$id,
                'uraian'=>$request->uraian,
                'jumlah'=>$request->jumlah,
                'kode_unit' => $request->kode_unit,
                'kode_barang'=>$request->kode_barang
            ]);
                
        }

        return redirect('permintaan/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permintaan::findOrFail($id)->delete();
        return redirect('permintaan');
    }

    public function approval($id)
    {
        Permintaan::where('id',$id)->update(['status'=>1]);
        return redirect('permintaan');
    }
}
