<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;
class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'profil';
        $profil = Profil::first();
        return view('profil.index',compact('profil','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'profil';
        $title = 'Profil';
        
        return view('profil.create',compact('page','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        
        
        return redirect('profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'profil';
        $title = 'Profil';
        $profil = Profil::find($id);
        return view('profil.edit',compact('page','title','profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        $profil = Profil::find($id);
        $pk = 0;
        if($request->pakai_logo == 'on'){
            $pk = 1;
        }

        $exists = file_exists(public_path('images/profil/'.$profil->logo));
        $logo = $profil->logo;
       
        
        if($exists && $request->file('logo') != null){
            $this->validate($request,[
                'logo'=>'required|mimes:jpeg,jpg,png|max:5000'
               
            ]);
            unlink(public_path('images/profil/'.$profil->logo));
           
        }
        if($request->file('logo') != null) {
            $logo = $request->nama_profil.'.png';
            $request->file('logo')->storeAs('profil',$logo,'public_img');
        }
        Profil::where('id',$id)->update([
            'nama' => $request->nama_profil,
            'logo' => $logo,
            'pakai_logo' => $pk,
            'alamat' => $request->alamat,
            'nama_bank'=>$request->nama_bank,
            'rekening_bank'=>$request->rekening_bank,
            'direktur'=>$request->direktur
            
        ]);
        return redirect('profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profil::where('id',$id)->delete();
        return redirect('profil');
    }
}
