<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\LevelUser;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'user';
        $users = User::all();
        return view('user.index',compact('users','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'user';
        $title = 'User';
        $level = LevelUser::all();
        return view('user.create',compact('page','title','level_user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'name' => 'required|max:255',
            'level_user_id' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'level_user_id' => $request->level,
            'password' => bcrypt($request->password),
        ]);
        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'user';
        $title = 'User';
        $user = User::find($id);
        $level = LevelUser::all();
        return view('user.edit',compact('page','title','user','level_user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id',$id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'level_user_id' => $request->level,
            
        ]);
        if($request->old_password){
            User::where('id',$id)->update([
                
                'password' => bcrypt($request->new_password),
                
            ]);
        }
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();
        return redirect('user');
    }
}
