<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Transfer;
use App\Akun;
use App\Kas;
class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'transfer';
        $title = 'Transfer';
     
        $transfers = Transfer::with('kas1','kas2','user')->get();
        //dd($transfers);
        return view('transfer.index',compact('transfers','page','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'transfer';
        $title = 'Transfer';
       
        $kas = Kas::where('transfer',1)->where('aktif',1)->get();

        return view('transfer.create',compact('page','kas','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
      
        Transfer::create([
                            
                            'tanggal'=>$request->tanggal,
                            'uraian'=>$request->uraian,
                            'kas_1_id'=>$request->kas1,
                            'kas_2_id'=>$request->kas2,
                            'jumlah'=>$request->jumlah,
                            'user_id'=>Auth::user()->id
                        ]);
        return redirect('transfer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'transfer';
        $title = 'Transfer';
        $transfer = Transfer::find($id);
        
        $kas = Kas::where('transfer',1)->where('aktif',1)->get();
        return view('transfer.edit',compact('page','title','transfer','kas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $page = 'transfer';
        $title = 'Transfer';
        $transfer = Transfer::find($id);
       

        Transfer::where('id',$id)->update([
                
                'tanggal'=>$request->tanggal,
                'uraian'=>$request->uraian,
                'kas_1_id'=>$request->kas1,
                'kas_2_id'=>$request->kas2,
                'jumlah'=>$request->jumlah,
                'user_id'=>Auth::user()->id
        ]);
        return redirect('transfer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transfer::find($id)->delete();
        return redirect('transfer');
    }
}
