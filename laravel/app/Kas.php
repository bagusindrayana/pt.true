<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kas extends Model
{
    protected $table = 'data_kas';
    protected $guarded = [];

    public function pemasukan(){
        return $this->hasMany('App\Pemasukan','kas_id');
    }

    public function pengeluaran(){
        return $this->hasMany('App\Pengeluaran','kas_id');
    }

    public function Transfers_1(){
        return $this->hasMany('App\Transfer','kas_1_id');
    }

    public function Transfers_2(){
        return $this->hasMany('App\Transfer','kas_2_id');
    }
}
