<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPemasukan extends Model
{	

	protected $guarded = [];

    public function pemasukan(){
    	return $this->hasMany('App\Pemasukan','id_pemasukan','id');
    }

    public function unit(){
        return $this->belongsTo('App\Unit','kode_unit','kode_unit');
    }
}
