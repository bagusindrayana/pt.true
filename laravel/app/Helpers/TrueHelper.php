<?php
namespace App\Helpers;
use App\Barang;
use App\Unit;

class TrueHelper {
	public static function harga_dasar($kdbarang)
  {
    $get = Barang::where('kode_barang',$kdbarang)->get();
    if ($get->count() > 0) {
    	foreach ($get as $get) 
	    {
	    	$result = $get->harga;
	    }
    }
    else
    {
    	$result = 0;
    }

    return $result;
  }

  public static function nama_barang($kdbarang)
  {
    $get = Barang::where('kode_barang',$kdbarang)->get();
    if ($get->count() > 0) {
    	foreach ($get as $get) 
	    {
	    	$result = $get->nama_barang;
	    }
    }
    else
    {
    	$result = '-';
    }

    return $result;
  }

  public static function nama_unit($kdunit)
  {
    $get = Unit::where('kode_unit',$kdunit)->get();
    if ($get->count() > 0) {
    	foreach ($get as $get) 
	    {
	    	$result = $get->nama_unit;
	    }
    }
    else
    {
    	$result = '-';
    }

    return $result;
  }
}