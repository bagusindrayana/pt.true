<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costomer extends Model
{
    protected $guarded = [];

    public function pemasukan(){
    	return $this->hasMany('App\Pemasukan','id_costomer','id');
    }

    public function tagihan(){
    	return $this->hasMany('App\Tagihan','id_costomer','id');
    }
}
