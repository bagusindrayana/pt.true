<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pembelian extends Model
{
    protected $guarded = [];

    public function permintaan(){
        return $this->belongsTo('App\Permintaan');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function kode()
    {
    	$query = DB::table('pembelians')->select(DB::raw('RIGHT(pembelians.nomor,4) as kode', FALSE))
                                       ->orderBy('nomor','DESC')
                                       ->limit(1)
                                       ->get();
      $count = count($query);
      if($count <> 0)
      {
          // $data = $query->toArray();
          $kode = intval($query[0]->kode) + 1;
      }
      else
      {
          $kode = 1;
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $kodejadi = "PO-".$kodemax;
      return $kodejadi;
    }
}
