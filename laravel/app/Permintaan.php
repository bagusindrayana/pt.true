<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Permintaan extends Model
{
    protected $guarded = [];

    public function diminta(){
        return $this->belongsTo('App\User','diminta_id','id');
    }


    public function detail(){
        return $this->hasMany('App\DetailPermintaan','permintaan_id','id');
    }

    public function delete(){
    	$this->detail()->delete();
    	parent::delete();
    }

    public function kode()
    {
        $query = DB::table('permintaans')->select(DB::raw('RIGHT(permintaans.nomor,4) as kode', FALSE))
                                         ->orderBy('nomor','DESC')
                                         ->limit(1)
                                         ->get();
        $count = count($query);
        if($count <> 0)
        {
            // $data = $query->toArray();
            $kode = intval($query[0]->kode) + 1;
        }
        else
        {
            $kode = 1;
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "PR-".$kodemax;
        return $kodejadi;
    }
}
