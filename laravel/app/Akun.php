<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Akun extends Model
{	

	
	protected $table = 'daftar_akuns';
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function pemasukans1(){
        return $this->hasMany('App\Pemasukan','id_akun_1','id');
    }

    public function pemasukans2(){
        return $this->hasMany('App\Pemasukan','id_akun_2','id');
    }

    public function pengeluarans1(){
        return $this->hasMany('App\Pengeluaran','id_akun_1','id');
    }

    public function pengeluarans2(){
        return $this->hasMany('App\Pengeluaran','id_akun_2','id');
    }

    
}
