<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $guarded = [];

	public function user(){
		return $this->belongsTo('App\User','user_id');
	}

	public function kas1(){
		return $this->belongsTo('App\Kas','kas_1_id');
	}

	public function kas2(){
		return $this->belongsTo('App\Kas','kas_2_id');
	}
}
