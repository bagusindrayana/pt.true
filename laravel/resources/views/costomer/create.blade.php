@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Customer</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Customer</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data Customer
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        <div class="form-group">
                            <label for="nama_costomer" class="form-label">Nama Customer</label>
                            <input required type="text" class="form-control" id="nama_costomer" name="nama_costomer">
                        </div>

                        <div class="form-group">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input required type="text" class="form-control" id="alamat" name="alamat">
                        </div>

                        <div class="form-group">
                            <label for="no_telepon" class="form-label">No Telepon</label>
                            <input required type="text" class="form-control" id="no_telepon" name="no_telepon">
                        </div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection