 <?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_neraca_".date('F')."-".date("Y_m_d_h_i_s").".xls"); 
 ?>
 <table style="text-align: center;">
     <tr >
         <td colspan="3" >PT. TRAS RENTAL UTAMA EKAMULYA</td>
     </tr>
     <tr>
         <td colspan="3">Neraca</td>
     </tr>

     <tr>
         <td colspan="3">Periode {{ date('F') }} {{ date('Y') }}</td>
     </tr>
 </table>
 <table >
    <thead>
        <tr>
            <th>Nomor Akun</th>
            <th>Nama Akun</th>     
            
            <th>Saldo</th>
          
        </tr>
    </thead>
    <tbody>
        <?php $total = 0; ?>
        @foreach($akuns as $data)
            @if($data->sub_akun == 0)
                <?php $total = 0; ?>
                <tr style="font-weight: bold;">
                   
                    <td colspan="2">{{ $data->nama_akun }} : </td>
                    
                    <td></td>
                
                
                </tr>
                
                @foreach($akuns as $sub_data)
                    @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                        <tr>
                            
                            
                            <td colspan="2" style="font-weight: bold;">{{ $sub_data->nama_akun }}</td>
                            
                            <td></td>
                        </tr>
                        
                        @foreach($akuns as $sub_sub_data)

                            @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0 )
                                <?php $total += (int)$sub_sub_data->saldo;?>
                                <tr>
                                    
                                    <td >{{ $sub_sub_data->nomor_akun }}</td>

                                    <td >{{ $sub_sub_data->nama_akun }}</td>
                                   
                                    <td>
                                        <div style="text-align: right; padding-right: 10%">Rp. {{ number_format($sub_sub_data->saldo) }}</div>
                                    </td>
                                  
                                </tr>
                                
                                @foreach($akuns as $sub_sub_sub_data)
                                    @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                        
                                        <tr>
                                            
                                            <td >{{ $sub_sub_sub_data->nomor_akun }}</td>

                                            <td ">{{ $sub_sub_sub_data->nama_akun }}</td>
                                            
                                            <td>Rp.{{ number_format($sub_sub_sub_data->saldo) }}</td>
                                           
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
                <tr >
                    <td colspan="3" style="font-weight: bold;text-align: center;border:1px solid black;">Total {{ $data->nama_akun }} : Rp.{{ number_format($total) }}</td>
                </tr> 
            @endif
        @endforeach
        
    </tbody>
    
</table>