@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Daftar Akun</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Daftar Akun</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data Akun
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$akun->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="nomor_akun" class="form-label">Nomor Akun</label>
                            <input type="text" class="form-control" id="nomor_akun" name="nomor_akun" value="{{ $akun->nomor_akun }}">
                        </div>

                        <div class="form-group">
                            <label for="nama_akun" class="form-label">Nama Akun</label>
                            <input type="text" class="form-control" id="nama_akun" name="nama_akun" value="{{ $akun->nama_akun }}">
                        </div>

                    

                        <div class="form-group">
                            <label for="jenis_akun" class="form-label">Jenis Akun</label>
                            <select class="form-control" id="jenis_akun" name="jenis_akun">
                                <option>Jenis Akun</option>
                                <option value="1" @if($akun->jenis_akun == 1) selected @endif>Aktiva</option>
                                <option value="2" @if($akun->jenis_akun == 2) selected @endif>Pasiva</option>
                                <option value="3" @if($akun->jenis_akun == 3) selected @endif>Ekuitas</option>
                                <option value="4" @if($akun->jenis_akun == 4) selected @endif>Pendapatan</option>
                                <option value="5" @if($akun->jenis_akun == 5) selected @endif>Biaya-Biaya</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipe_akun" class="form-label">Tipe Akun</label>
                            <select class="form-control" id="tipe_akun" name="tipe_akun">
                                <option>Tipe Akun</option>
                                <option value="1" @if($akun->tipe_akun == 1) selected @endif>Cash/Bank</option>
                                <option value="2" @if($akun->tipe_akun == 2) selected @endif>Non Cash/Bank</option>
                               
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="kategori_akun" class="form-label">Kategori Akun</label>
                            <select class="form-control" id="kategori_akun" name="kategori_akun">
                                <option>Kategori Akun</option>
                                <option value="1" @if($akun->kategori_akun == 1) selected @endif>General</option>
                                <option value="2" @if($akun->kategori_akun == 2) selected @endif>Detail</option>
                               
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="sub_akun"  class="form-label">Sub Akun</label>
                            <select class="form-control" id="sub_akun" name="sub_akun">
                                <option value="0">Tidak Ada</option>
                                @foreach($sub_akun as $d)
                                    <option value="{{ $d->id }}" @if($d->id == $akun->sub_akun) selected @endif>{{ $d->nomor_akun }} | {{ $d->nama_akun }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipe_saldo" class="form-label">Tipe Saldo</label>
                            <select class="form-control" id="tipe_saldo" name="tipe_saldo">
                                <option value="1"  @if($akun->tipe_saldo == 1) selected @endif>Debit</option>
                                <option value="2"  @if($akun->tipe_saldo == 1) selected @endif>Kredit</option>
                            </select>
                        </div>

                     

                        <div class="form-group">
                            <label for="saldo" class="form-label">Saldo</label>
                            <input type="number" class="form-control" id="saldo" name="saldo"  value="{{ $akun->saldo }}">
                        </div>
                        
                        <button type="submit" class="btn btn-default">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection