<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>TRUE | Login Page</title>
    <!-- Css Files -->
    <link href="css/root.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
</head>
<?php use App\Profil;
    $profilnya = Profil::first();
 ?>

<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h2 class="logo-name">{{ $profilnya->nama }}</h2>
                @if($profilnya->logo != '' && $profilnya->pakai_logo == 1) 
                    <img src="{{ asset('images/profil/'.$profilnya->logo)  }}" style="max-width: 40%;">
                @endif
            </div>
            
            
            <form class="m-t" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" name="email" required="">
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="">
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <br>
                
            </form>
            <p class="m-t"> <small>Greennusa Computindo &copy; 2018</small> </p>
        </div>
    </div>
</body>
</html>

