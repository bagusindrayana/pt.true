@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>
<form action="{{ url($page.'/'.$jurnalumum->id) }}" method="POST" id="form_jurnal">
<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="_method" value="PUT">
                        

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $jurnalumum->tanggal }}"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-group">
                            <label for="no_jurnal" class="form-label" >No Jurnal</label>
                            <input type="text" name="no_jurnal" class="form-control" required="" value="{{ $jurnalumum->no_jurnal }}">
                
                        </div> 
                        

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input required type="text" class="form-control" id="uraian" name="uraian" value="{{ $jurnalumum->uraian }}">
                        </div>
                        <div style="clear: both;"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    
                    <table  class="table display">
                        <thead>
                            <tr>
                                <th>Akun</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                                <th>Uraian</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="detail_jurnal">
                            
                            <?php $total_debit = 0;$total_kredit = 0;$i = 1; ?>
                            @foreach($jurnalumum->detail as $data)
                                <tr  id="list-{{ $i }}" class="listnya">
                                    <td>
                                        <select name="id_akun[]" class="form-control pilih-akun" required style="font-size: 13px;width: 200px;">
                                            <option value="0">Pilih Akun</option>
                                            @foreach($akuns as $akun)
                                                <option @if($data->id_akun == $akun->id) selected @endif value="{{ $akun->id }}">{{ $akun->nomor_akun }} | {{ $akun->nama_akun }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" name="debit[]"  min="0"  class="input_debit" value="{{ $data->debit }}">
                                    </td>
                                    <td>
                                        <input type="number" name="kredit[]"  min="0" class="input_kredit" value="{{ $data->kredit }}">
                                    </td>
                                    <td>
                                        <textarea class="uraian" name="uraian_detail[]">{{ $data->uraian }}</textarea>
                                        
                                    </td>
                                    <td>

                                        <span @if($i == 1) style="display: none;" @endif class="fa fa-trash del_list" list-id="list-1"></span>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                <?php $total_debit += $data->debit;$total_kredit += $data->kredit; ?>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td><input disabled  readonly type="number" id="total_debit" name="total_debit" value="{{ $total_debit }}" min="0"  readonly=""></td>
                                <td><input disabled  readonly type="number" id="total_kredit" name="total_kredit" value="{{ $total_kredit }}" min="0"  readonly=""></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        
                    </table>
                    <button type="button" id="add_item">Akun Baru</button>
                    <br>
                    <br>
                    <label>Out of Balance : </label> <input type="number" name="ofb" value="0" readonly min="0" id="ofb">
                    <br>
                    <br>
                    <button type="submit" class="btn btn-default">Tambah</button>
                    
                    
                </div>
            </div>
        </div>
    </div>

</div>
</form>

@endsection