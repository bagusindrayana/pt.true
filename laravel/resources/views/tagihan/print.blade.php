<?php 
use App\Tagihan;
$profil = \App\Profil::first();
$tagihan = Tagihan::find($id); 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment;filename=".$tagihan->invoice.date("Y_m_d_h_i_s").".xls"); 
 ?>              
 <style type="text/css">
     tr {
        text-align: center;
     }
 </style>
    <center>
                <table style="text-align: center;">
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr style="text-align: center;"><td colspan="9" ><h1>PT. TRAS RENTAL UTAMA EKAMULYA</h1></td></tr>
                    <tr style="text-align: center;"><td colspan="9" ><h4>Jl. Jakarta 2 No.8 Kel. Loa Bakung Kec. Sungai Kunjang</h4></td></tr>
                    <tr style="text-align: center;"><td colspan="9" ><h4>Samarinda - Kalimantan Timur</h4></td></tr>
            
                </table>
            </center>
                
            
            <table>
                <tr><td></td></tr>
                <tr><td></td></tr>

                <tr><td></td><td>To : {{ $tagihan->costomer->nama_costomer }}</td></tr>
           
               
                
               
            </table>
           
                <table style="float: right;">
                    
                    <tr><td colspan="5">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td ><h2>INVOICE</h2></td></tr>
                    <tr><td colspan="5">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td >No : {{ $tagihan->no }}</td></tr>
                    <tr><td colspan="5">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td >Site : {{ $tagihan->site }}</td></tr>
                    <tr><td colspan="5"><br><br><br></td></tr>
                </table>
                    <?php $no = 1; ?>
                    <table style="border: 1px solid black;width: 100%;">
                        
                            <tr>
                                <th style="border: 1px solid black;width: 10%">NO</th>
                                <th  style="border: 1px solid black;">Unit</th>
                                <th colspan="2" style="border: 1px solid black;width: 300px;">Uraian</th>
                                <th style="border: 1px solid black;">QTY</th>
                                <th colspan="3" style="border: 1px solid black;">Harga Satuan</th>
                                <th style="border: 1px solid black;">Jumlah</th>
                               
                            </tr>
                        
                       
                            <?php $total = 0; ?>
                            @foreach($tagihan->detail as $data)
                                <tr>
                                    <td style="border-right: 1px solid black;">{{ $no }}</td>
                                    <td  style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->unit->nama_unit }}</td>
                                    <td colspan="2" style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->uraian }}</td>
                                    <td style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->qty }}</td>
                                    <td colspan="3"style="border-right: 1px solid black;border-bottom: 1px solid black;">Rp.{{  number_format($data->harga_satuan,2) }}</td>
                                    <td style="border-right: 1px solid black;border-bottom: 1px solid black;">Rp.{{  number_format($data->jumlah,2) }}</td>
                                    
                                    
                                </tr>
                                <?php $total += $data->jumlah;$no++; ?>
                            @endforeach
                            <?php 
                                $total_semua = $total;
                                
                                
                     

                             ?>
                            <tr>
                                <td style="border-right: 1px solid black;"></td>
                                <td colspan="3" style="border-right: 1px solid black;"></td>
                                <td  style="border-right: 1px solid black;"></td>
                                <td colspan="3"style="border-right: 1px solid black;"></td>
                                <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="border-right: 1px solid black;"></td>
                                <td colspan="3" style="border-right: 1px solid black;"></td>
                                <td  style="border-right: 1px solid black;"></td>
                                <td colspan="3"style="border-right: 1px solid black;"></td>
                                <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="border-right: 1px solid black;"></td>
                                <td colspan="3" style="border-right: 1px solid black;"></td>
                                <td  style="border-right: 1px solid black;"></td>
                                <td colspan="3"style="border-right: 1px solid black;"></td>
                                <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3" style="border-right: 1px solid black;"></td>
                                    <td  style="border-right: 1px solid black;"></td>
                                <td colspan="3"style="border-right: 1px solid black;">DPP : </td>
                                <td style="border-right: 1px solid black;">Rp.{{  number_format($total,2) }}</td>
                            </tr>
                            @if($tagihan->ppn == 1)
                            <?php $ppn = $total*(10/100);$total_semua; ?>
                                <tr>
                                    <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3" style="border-right: 1px solid black;"></td>
                                    <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3"style="border-right: 1px solid black;">PPN 10% : </td>
                                    <td style="border-right: 1px solid black;">Rp.{{  number_format($ppn,2) }}</td>
                                </tr>
                            @endif
                            @if($tagihan->pph == 1)
                            <?php $pph = $total*(2/100);$total_semua ; ?>
                                <tr>
                                    <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3" style="border-right: 1px solid black;"></td>
                                    <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3"style="border-right: 1px solid black;">PPh 2% : </td>
                                    <td style="border-right: 1px solid black;">Rp.{{ number_format($pph,2) }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td style="border-right: 1px solid black;"></td>
                                    <td colspan="3" style="border-right: 1px solid black;"></td>
                                    <td style="border-right: 1px solid black;"></td>
                                <td colspan="3"style="border-right: 1px solid black;">Total : </td>
                                <td style="border-right: 1px solid black;">Rp.{{ number_format($total_semua+$ppn-$pph,2) }}</td>
                            </tr>
                       
                        
                    </table>
                    <br>
                    <br>
                    <br>
                    <table>
                        <tr style="text-align: right;"><td></td><td colspan="8">Samarinda, {{ date('d F Y',strtotime($tagihan->tanggal)) }}</td></tr>
                        <tr style="text-align: left;"><td></td><td >PT. TRAS RENTAL UTAMA EKAMULYA</td></tr>
                    </table>
                    
                    <br>
                    <br>
                    <br>
                    <table style="float: right;" >
                    
                    <tr style="text-align: left;"><td></td><td colspan="4">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td colspan="4">Pembayaran bisa ditransfer ke rekening : </td></tr>
                    <tr style="text-align: left;"><td></td><td colspan="4">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td >Nama        :  PT. TRAS RENTAL UTAMA EKAMULYA</td></tr>
                    <tr style="text-align: left;"><td></td><td colspan="4">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td >Bank          :  {{$profil->nama_bank}}</td></tr>
                    <tr style="text-align: left;"><td></td><td colspan="4">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td >No.Rek      :  {{$profil->rekening_bank}}</td></tr>
                
                </table>
                <div style="float: left;clear: both;">
                       {{$profil->direktur}}
                       <br>
                       Direktur
                    </div>


            