@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{$title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Inventory</a></li>
        <li class="breadcrumb-item active">Data {{$title}}</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"> </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <?php $no = 1; ?>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor</th>
                                <th>Nomor Permintaan</th>
                                <th>Tanggal</th>
                                <th>Di Minta Oleh</th>
                                <th>Di Setujui Oleh</th>

                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembelians as $data)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $data->nomor }}</td>
                                    <td>{{ $data->permintaan->nomor }}</td>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>{{ $data->permintaan->diminta->name }}</td>
                                    <td>{{ $data->user->name }}</td>
                                    <td class="aksi">
                                        <?php if($data->status == 1) { ?>
                                        <a href="{{ url($page.'/'. $data->id) }}" class="fa fa-info"></a>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete<?php echo $data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="delete<?php echo $data->id;  ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                        <?php } else { ?>
                                        <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a>
                                        <a href="{{ url($page.'/'. $data->id) }}" class="fa fa-info"></a>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete<?php echo $data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="delete<?php echo $data->id;  ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                        <?php } ?>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection