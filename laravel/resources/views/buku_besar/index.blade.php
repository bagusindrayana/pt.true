@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi Kas</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="nomor_akun" class=" col-md-2" >Dari Tanggal :</label>
                            <input type="date" class=" col-md-2" name="dari_tanggal">
                            <label for="nomor_akun" class=" col-md-2" >Sampai Tanggal :</label>
                            <input type="date" class=" col-md-2" name="sampai_tanggal"> 
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                <div class="panel-body table-responsive">
                    
                    <table class="table display">
                        <thead>
                            <tr>
                                <th>Nomor/Tanggal</th>
                                <th>Transaksi</th>
                                <th>Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($akun as $data)
                                <tr>
                                    <td>{{ $data->nomor_akun }}</td>
                                    <td>{{ $data->nama_akun }}</td>
                                    <td>Saldo Awal : {{ $data->saldo }}</td>
                                </tr>

                                @if(count($data->pemasukans1) > 0)
                                    @foreach($data->pemasukans1 as $dd)
                                        <tr >
                                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                                            <?php $total = 0; ?>
                                            @foreach($dd->detail as $d)
                                                <?php $total += $d->jumlah; ?>
                                            @endforeach
                                            <td>Pemasukan {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                                            <td>Rp.{{ number_format($total) }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if(count($data->pemasukans2) > 0)
                                    @foreach($data->pemasukans2 as $dd)
                                        <tr >
                                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                                            <?php $total = 0; ?>
                                            @foreach($dd->detail as $d)
                                                <?php $total += $d->jumlah; ?>
                                            @endforeach
                                            <td>Pemasukan {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                                            <td>Rp.{{ number_format($total) }}</td>
                                        </tr>
                                    @endforeach
                                @endif


                                @if(count($data->pengeluarans1) > 0)
                                    @foreach($data->pengeluarans1 as $dd)
                                        <tr >
                                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                                            <?php $total = 0; ?>
                                            @foreach($dd->detail as $d)
                                                <?php $total += $d->jumlah; ?>
                                            @endforeach
                                            <td>Pengeluaran {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                                            <td>Rp.{{ number_format($total) }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if(count($data->pengeluarans2) > 0)
                                    @foreach($data->pengeluarans2 as $dd)
                                        <tr >
                                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                                            <?php $total = 0; ?>
                                            @foreach($dd->detail as $d)
                                                <?php $total += $d->jumlah; ?>
                                            @endforeach
                                            <td>Pengeluaran {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                                            <td>Rp.{{ number_format($total) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach

                          
                            
                        </tbody>
                        
                    </table>
                    {{$akun->appends(request()->input())->render()}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection