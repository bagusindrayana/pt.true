<?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_buku_besar-".date("Y_m_d_h_i_s").".xls"); 
 ?>
 <table>
     <tr>
         <td>{{ $from }} - {{ $to }}</td>
     </tr>
     <tr>
         <td><br><br><br></td>
     </tr>
 </table>
<table border="1">
    <thead>
        <tr>
            <th>Nomor/Tanggal</th>
            <th>Transaksi</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
        @foreach($akun as $data)
            <tr style="background-color: grey;font-weight: bold;">
                <td>{{ $data->nomor_akun }}</td>
                <td>{{ $data->nama_akun }}</td>
                <td>Saldo Awal : {{ $data->saldo }}</td>
            </tr>

            @if(count($data->pemasukans1) > 0)
                @foreach($data->pemasukans1 as $dd)
                    @if($dd->tanggal >= $from && $dd->tanggal <= $to)
                        <tr >
                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                            <?php $total = 0; ?>
                            @foreach($dd->detail as $d)
                                <?php $total += $d->jumlah; ?>
                            @endforeach
                            <td>Pemasukan {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                            <td>Rp.{{ number_format($total) }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif

            @if(count($data->pemasukans2) > 0)
                @foreach($data->pemasukans2 as $dd)
                    @if($dd->tanggal >= $from && $dd->tanggal <= $to)
                        <tr >
                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                            <?php $total = 0; ?>
                            @foreach($dd->detail as $d)
                                <?php $total += $d->jumlah; ?>
                            @endforeach
                            <td>Pemasukan {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                            <td>Rp.{{ number_format($total) }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif


            @if(count($data->pengeluarans1) > 0)
                @foreach($data->pengeluarans1 as $dd)
                    @if($dd->tanggal >= $from && $dd->tanggal <= $to)
                        <tr >
                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                            <?php $total = 0; ?>
                            @foreach($dd->detail as $d)
                                <?php $total += $d->jumlah; ?>
                            @endforeach
                            <td>Pengeluaran {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                            <td>Rp.{{ number_format($total) }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif

            @if(count($data->pengeluarans2) > 0)
                @foreach($data->pengeluarans2 as $dd)
                    @if($dd->tanggal >= $from && $dd->tanggal <= $to)
                        <tr >
                            <td style="padding-left: 50px;">{{ $dd->tanggal }}</td>
                            <?php $total = 0; ?>
                            @foreach($dd->detail as $d)
                                <?php $total += $d->jumlah; ?>
                            @endforeach
                            <td>Pengeluaran {{ $dd->akun1->nama_akun }} ke {{ $dd->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                            <td>Rp.{{ number_format($total) }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
        @endforeach

      
        
    </tbody>
    
</table>