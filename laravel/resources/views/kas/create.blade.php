@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Kas</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Kas</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data Kas
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        <div class="form-group">
                            <label for="nama_kas" class="form-label">Nama Kas</label>
                            <input required type="text" class="form-control" id="nama_kas" name="nama_kas">
                        </div>

                        <div class="form-group">
                            <label for="aktif" class="form-label">Aktif</label>
                            <select required class="form-control" id="aktif" name="aktif">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="penarikan"  class="form-label">Penarikan</label>
                            <select required class="form-control" id="penarikan" name="penarikan">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="pemasukan" class="form-label">Pemasukan</label>
                            <select required class="form-control" id="pemasukan" name="pemasukan">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="transfer" class="form-label">Transfer</label>
                            <select required class="form-control" id="transfer" name="transfer">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection