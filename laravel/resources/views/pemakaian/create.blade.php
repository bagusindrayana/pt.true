@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        
                       {{--  <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input type="text" name="nomor" class="form-control">
                
                        </div>  --}}


                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo date('Y-m-d') ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                                                
                        
                        
                     

                        <div class="form-group">
                            <label for="user_id" class="form-label" >Di Pakai Oleh</label>
                            <select name="user_id" class="form-control">
                                @foreach($user as $data)
                                    
                                        <option  value="{{ $data->id }}">{{ $data->name }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>


                        <div class="form-group">
                            <label for="untuk" class="form-label" ></label>
                            <h5>Detail Pemakaian</h5>
                
                        </div> 

                        <div class="form-group">
                            <label for="uraian" class="form-label" >Uraian</label>
                            <input type="text" name="uraian" class="form-control">
                
                        </div>

                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $data)
                                    
                                        <option  value="{{ $data->kode_unit }}">{{ $data->nama_unit }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>


                        <div class="form-group">
                            <label for="kode_barang" class="form-label" >Barang</label>
                            <select name="kode_barang" class="form-control">
                                <option>Pilih Barang</option>
                                @foreach($barangs as $data)
                                    
                                        <option  value="{{ $data->kode_barang }}">{{ $data->kode_barang }} - {{ $data->barang->nama_barang }} - stok : {{$data->jumlah}}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="jumlah" class="form-label" >Jumlah</label>
                            <input type="number" name="jumlah" class="form-control">
                
                        </div> 
                        
                        
                        
                       
                       

                       
                        <div style="clear: both;"></div>
                        <a href="{{ url('pemakaian') }}" class="btn btn-info">Kembali</a>
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection