<?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_transaksi_kas-".date("Y_m_d_h_i_s").".xls"); 
 ?>
<table border="1">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Jenis Transaksi</th>
            <th>Transaksi</th>
            
           
        </tr>
    </thead>
    <tbody>
        @foreach($tagihan as $data)
            <tr>
                <td>{{ $data->tanggal }}</td>
                <td>Tagihan</td>
                <td>Tagihan {{ $data->unit->nama_unit }} ke {{ $data->costomer->nama_costomer }}</td>
                
            </tr>
        @endforeach

        @foreach($pengeluaran as $data)
            <tr>
                <td>{{ $data->tanggal }}</td>
                <td>Pengeluaran</td>
                <?php $total = 0; ?>
                @foreach($data->detail as $d)
                    <?php $total += $d->jumlah; ?>
                @endforeach
                <td>Pengeluaran {{ $data->akun1->nama_akun }} ke {{ $data->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                
            </tr>
        @endforeach


        @foreach($pemasukan as $data)
            <tr>
                <td>{{ $data->tanggal }}</td>
                <td>Pemasukan</td>
                <?php $total = 0; ?>
                @foreach($data->detail as $d)
                    <?php $total += $d->jumlah; ?>
                @endforeach
                <td>Pemasukan {{ $data->akun1->nama_akun }} ke {{ $data->akun2->nama_akun }} Rp.{{ number_format($total) }}</td>
                
            </tr>
        @endforeach
        
    </tbody>
    
</table>