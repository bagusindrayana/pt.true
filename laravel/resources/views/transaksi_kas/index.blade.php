@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi Kas</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="nomor_akun" class=" col-md-2" >Dari Tanggal :</label>
                            <input type="date" class=" col-md-2" name="dari_tanggal">
                            <label for="nomor_akun" class=" col-md-2" >Sampai Tanggal :</label>
                            <input type="date" class=" col-md-2" name="sampai_tanggal"> 
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Jenis Transaksi</th>
                                
                                
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tagihan as $data)
                                <tr>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>Tagihan</td>
                                    
                                    
                                </tr>
                            @endforeach

                            @foreach($pengeluaran as $data)
                                <tr>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>Pengeluaran</td>
                                    
                                    
                                </tr>
                            @endforeach


                            @foreach($pemasukan as $data)
                                <tr>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>Pemasukan</td>
                                    
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection