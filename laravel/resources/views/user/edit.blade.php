
@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">

                    <form class="form-horizontal" method="POST" action="{{ url($page.'/'.$user->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama</label>

                            
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="level" class="form-label">Level User</label>
                            <select required class="form-control" id="level" name="level">
                                @foreach($level as $d)
                                    <option value="{{ $d->id }}"  @if($user->level_user_id == $d->id) selected @endif>{{ $d->nama_level }}</option>
                                @endforeach
                                
                            </select>
                            @if ($errors->has('level'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        @if($user->id == Auth::user()->id)
                            <div class="form-group">
                                
                                    <button type="button" class="btn btn-primary" id="btn_ubah_pass">
                                        Ubah Password
                                    </button>
                                
                            </div>

                            <div style="display: none;" id="edit_pass">
                                <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                    <label for="old_password" class="col-md-4 control-label">Password Lama</label>

                                    
                                        <input id="old_password" type="password" class="form-control" name="old_password" required>

                                        @if ($errors->has('old_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                        @endif
                                    
                                </div>

                                <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                    <label for="new_password" class="col-md-4 control-label">Password Baru</label>

                                    
                                        <input id="new_password" type="password" class="form-control" name="new_password" required>

                                        @if ($errors->has('new_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('new_password') }}</strong>
                                            </span>
                                        @endif
                                    
                                </div>

                                

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                    
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            
                                <button type="submit" class="btn btn-secondary">
                                    Edit
                                </button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<script type="text/javascript">
    var btn = document.getElementById('btn_ubah_pass');
    var i = true;
    btn.onclick = function(){
        if(i){
            document.getElementById('edit_pass').style.display = '';
            document.getElementById('old_password').setAttribute('enabled','false');
            i = false;
        }
        else {
            document.getElementById('edit_pass').style.display = 'none';
            document.getElementById('old_password').setAttribute('enabled','true');
            i = true;
        }
    }
</script>

@endsection


