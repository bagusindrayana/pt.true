
@include('layouts.top')

@include('layouts.sidebar')

    <div class="page-content" > 
          @yield('content')   
	<!-- End Container --> 
	<!-- Start Footer -->
	<div class="row footer">
	    <div class="col-md-6 text-left"> Copyright &copy; 2018 Greennusa Computindo All rights reserved. </div>
	    <div class="col-md-6 text-right"> Design and Developed by Foxlabel </div>
	</div>
	<!-- End Footer --> 
	</div>
	<!-- End Content --> 
   

@include('layouts.bottom')