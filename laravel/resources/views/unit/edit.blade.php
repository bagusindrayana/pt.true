@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Unit</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Unit</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data Unit
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$unit->kode_unit) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="kode_unit" class="form-label">Kode Unit</label>
                            <input required type="text" class="form-control" id="kode_unit" name="kode_unit" value="{{ $unit->kode_unit }}">
                        </div>

                        <div class="form-group">
                            <label for="nama_unit" class="form-label">Nama Unit</label>
                            <input required type="text" class="form-control" id="nama_unit" name="nama_unit" value="{{ $unit->nama_unit }}">
                        </div>

                        <button type="submit" class="btn btn-default">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection