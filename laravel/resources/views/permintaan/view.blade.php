@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Lihat Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$data->id) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input disabled type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input disabled type="text" name="nomor" class="form-control" value="{{ $data->nomor }}">
                
                        </div> 


                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input disabled type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $data->tanggal }}"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                                                
                        
                        
                     

                        <div class="form-group">
                            <label for="diminta_id" class="form-label" >Di Minta Oleh</label>
                            <select name="diminta_id" class="form-control" disabled="">
                                @foreach($user as $dx)
                                    
                                        <option  value="{{ $dx->id }}">{{ $dx->name }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>
                        <div style="clear: both;"></div>
            
                        
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <?php $no = 1; ?>
                            <tr>
                                <th>No</th>
                            
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Unit</th>
                                <th>Jumlah</th>
                                <th>Urain</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($data->detail as $dd)
                            <?php $total += $dd->jumlah; ?>
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    
                                    <td>{{ $dd->barang->kode_barang }}</td>
                                    <td>{{ $dd->barang->nama_barang }}</td>
                                    <td>{{ $dd->unit->nama_unit }}</td>
                                    <td>{{ $dd->jumlah }}</td>
                                    <td>{{ $dd->uraian }}</td>
                                    
                                </tr>
                            @endforeach
                           <tr>
                               <td colspan="8" style="text-align: right;font-weight: bold;padding-right: 35%;">Total : {{ $total }}</td>
                           </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection