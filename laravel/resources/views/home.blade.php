@extends('layouts.template')

@section('content')


<!-- Start Container -->
            <div class="container-default animated fadeInRight"> <br>
                <!-- Start Row -->
                <div id="tour-12" class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-facebook rounded"> <span class="mini-stat-icon"><i class="fa fa-codepen fg-facebook"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $akuns }}">0</span> Daftar Akun </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-twitter rounded"> <span class="mini-stat-icon"><i class="fa fa-group fg-twitter"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $costomers }}">0</span> Costomer </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-googleplus rounded"> <span class="mini-stat-icon"><i class="fa fa-truck fg-googleplus"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $suppliers }}">0</span> Suplier </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-bitbucket rounded"> <span class="mini-stat-icon"><i class="fa fa-cubes fg-bitbucket"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $barangs }}">0</span> Barang </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <div class="mini-stat clearfix bg-twitter rounded"> <span class="mini-stat-icon"><i class="fa fa-pagelines fg-twitter"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $units }}">0</span> Unit </div>
                        </div>
                    </div>
                </div>
                <!-- End Row --> 

                <!-- Start Row -->
                <div id="tour-12" class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-facebook rounded"> <span class="mini-stat-icon"><i class="fa fa-pencil-square-o fg-facebook"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $tagihans }}">0</span> Tagihan </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-twitter rounded"> <span class="mini-stat-icon"><i class="fa fa-sign-in fg-twitter"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $pemasukans }}">0</span> Pemasukan </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-googleplus rounded"> <span class="mini-stat-icon"><i class="fa fa-sign-out fg-googleplus"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $pengeluarans }}">0</span> Pengeluaran </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="mini-stat clearfix bg-bitbucket rounded"> <span class="mini-stat-icon"><i class="fa fa-share fg-bitbucket"></i></span>
                            <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="0">0</span> Transfer </div>
                        </div>
                    </div>
                </div>
                <!-- End Row --> 

                
    
<div class="row"> 
    <!-- Start Left -->
    <div class="col-md-4">
        
        <div class="panel panel-widget">
            <div class="panel-title"> Inventory </div>
            <div class="panel-body">
                <ul class="basic-list">
                    <li>Permintaan <span class="right label label-default">0</span></li>
                    <li>Pembelian <span class="right label label-danger">0</span></li>
                    <li>Pemakaian <span class="right label label-success">0</span></li>
                    
                </ul>
            </div>
        </div>
        
    </div>
    <!-- End Left --> 
    <!-- Start Right -->
    <div class="col-md-4">
        <!-- Start Teammates -->
        <div class="panel widget panel-widget">
            <div class="panel-title"> User Pengguna Terbaru</div>
            <div class="panel-body">
                <ul class="basic-list image-list">
                    <?php 
                        foreach ($users as $data) {
                            ?>
                                <li><b>{{ $data->name }}</b><span class="desc">{{ $data->level_user->nama_level }}</span></li>
                            <?php
                        }
                     ?>
                    
                    
                </ul>
            </div>
        </div>
        <!-- End Teammates --> 
    </div>
    <!-- End Right --> 
    
</div>
<!-- End Row --> 
</div>  


@endsection